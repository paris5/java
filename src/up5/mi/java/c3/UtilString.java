package up5.mi.java.c3;

public class UtilString {
	
	/* 
	 * 
	 * @param
	 * @return
	 */
	public static String cadrerADroite(String chaine, int longueur)
	{
		if(chaine.length()==longueur) {
			return chaine;
		}
			
		if(chaine.length()>longueur)
		{
			chaine=chaine.substring(chaine.length()-longueur);
			return chaine;
		}
		else {
			StringBuffer sb = new StringBuffer();
			for(int i=0;i<longueur-chaine.length();i++) {
				sb.append("-");
			}				
			sb.append(chaine);
			return sb.toString();
		}
	}
	
	/* 
	 * 
	 * @param
	 * @return
	 */
	public static String cadrerAGauche(String chaine, int longueur)
	{
		if(chaine.length()==longueur) {
			return chaine;
		}
		
		if(chaine.length()>longueur)
		{
			chaine=chaine.substring(0, longueur);
			return chaine;
		}
		else{
			StringBuffer sb = new StringBuffer();
			sb.append(chaine);
			for(int i=0;i<longueur-chaine.length();i++) {
				sb.append("-");
			}
			return sb.toString();
		}
	}

}
	