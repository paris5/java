package up5.mi.java.B12;

import up5.mi.pary.term.Terminal;

public class B12 {

	
		/**
		 * Trouve un nombre compris entre min et max en utilisant le terminal.
		 * 
		 * @param term à utiliser pour le jeu
		 * @param min de l'interval
		 * @param max de l'interval
		 * @return le nombre de coup joué par l'ordinateur.
		 */
		public static int trouveNombre(Terminal term, int min, int max) {
			
			int compteur = 0;
			int minActuel=min;
			int maxActuel=max;
			
			char c;
			while(true) {
				// Utilisons la dichotomie pour faire une proposition
				int proposition = (minActuel + maxActuel)/2;
				
				// Maj compteur
				compteur++;

				// Proposons au joueur et attendons sa réponse 
				do {
					c = term.readChar("Je propose " + proposition + " Est-ce =, + ou - grand que le nombre à trouver ?");
				}
				while (c!='+' && c!='-' && c!='=');
				
				// Verifions la réponse
				switch(c){
					case '-':
						minActuel = proposition;
						break;
						
					case '+':
						maxActuel = proposition;
						break;
						
					case '=':
						return compteur;		
				}
			}
		}
		
			
	
		/**
		 * Lance le jeu.
		 * 
		 * @param args
		 */
		public static void main(String[] args) {
			
			Terminal term = new Terminal("Game", 600, 400);
			
			int nombreCoups = trouveNombre(term, 0, 100);
			
			term.println("j'ai trouvé en " + nombreCoups + " coups");
			
			term.end();
		
		}
}
