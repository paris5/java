package up5.mi.java.B11;

public class B11a {

	// B11 A
	public static boolean isPremier(int n) {
		if (n <= 1) {
			return false;
		}
		if (n == 2) {
			return true;
		}

		for (int i = 2; i < n; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	
	// B11 B
	public static boolean isPremier2(int n) {
		if (n <= 1) {
			return false;
		}
		if (n == 2) {
			return true;
		}

		if (n % 2 == 0) {
			return false;
		}

		for (int i = 3; i < n; i += 2) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	// B11 C
	public static boolean isPremier3(int n) {
		if (n <= 1) {
			return false;
		}
		if (n == 2) {
			return true;
		}

		if (n % 2 == 0) {
			return false;
		}

		for (int i = 3; i < Math.sqrt(n); i += 2) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	
	
	// Main
	public static void main(String[] args) {

		for (int i = 1; i < 10; i++) {
			System.out.println("" + i + " est premier: " + isPremier3(i));
		}

	}

}
