package up5.mi.java.B11;

import up5.mi.pary.term.Terminal;

public class B11b {

	
	public static void afficheNPremiers(Terminal term, int n) {
		term.println("Voici les "+ n + " nombres premiers à partir de 2");
		
		int found = 0;
		int current = 2;
		while(found<n) {
			if(B11a.isPremier(current)) {
				found++;
				term.println(current);
			}
			current++;
		}
	}
	
	
	
	public static void main(String[] args) {
		
		Terminal term = new Terminal("B11", 400, 400);
		
		afficheNPremiers(term, 10);
	}

}
