package up5.mi.java.c4;

public class C4b {

	public static String getChaineDesNombresDe1aN(int n){
		StringBuilder s= new StringBuilder(n);
		for(int i=1;i<n+1;i++){
			s.append(i);
		}
		return s.toString();
	}

	public static void main(String[] args) {
		System.out.println(getChaineDesNombresDe1aN(5)); 
	}
	
}
