package up5.mi.java.c4;

import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.math.BigDecimal;

public class C4c {

	/** 
	 * @return la factorielle de n
	 * */
	public static BigInteger factorielle (int n) {
		
		BigInteger bigInt = BigInteger.valueOf(1);
		for(int i=1;i<=n;i++) {
			bigInt = bigInt.multiply(BigInteger.valueOf(i));
		}
		
		return bigInt;
	}
	
	/**
	 * 
	 * @param n
	 */
	public static void afficherFact(int n) {
		
		// Calcul la factorielle.
		BigInteger bigInt = factorielle(n);
		
		// Affiche le BigInter
		System.out.println("fact(" + n + ")=" + bigInt);
		
		// Affiche l'arrondi du BigInt
		MathContext mc = new MathContext(17, RoundingMode.HALF_EVEN);
		System.out.println("fact(" + n + ")~=" + new BigDecimal(bigInt).setScale(1).round(mc));
	}
	
	
	public static void main(String [ ] args){
		afficherFact(0);
		afficherFact(7);
		afficherFact(70);
		}
	
}
