package up5.mi.java.c4;
import up5.mi.pary.jc.rationnel.Rationnel;

public class C4a {
	
	
	public static Rationnel puissanceNieme(Rationnel nbr, int puissance){

		Rationnel num = new Rationnel(1); 
		for (int i = 1;i<=puissance;i++){
			num = num.multiplication(nbr);
		}
		return num;
	}
	
	public static void main(String[] args) {
		
		Rationnel r = new Rationnel(1,3);
		System.out.println(puissanceNieme(r,2));
		System.out.println(puissanceNieme(r,5));
	}

}
