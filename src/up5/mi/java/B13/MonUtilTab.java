package up5.mi.java.B13;

public class MonUtilTab {

	/**
	 * Renvoi le nombre de nombre pairs dans le tableau.
	 * 
	 * @param tab
	 * @return nombre de pairs.
	 */
	public static int nombreDePair(int[] tab) {
		int nombrePairs = 0;
		for(int i : tab) {
			if(i%2 == 0) {
				nombrePairs++;
			}
		}
		return nombrePairs;
	}

	/**
	 * Calcul la somme des nombres contenu dans un tableau
	 * 
	 * @param tab1
	 * @param tab2
	 * @return null si la taille est différente, sinon le tableau des sommes.
	 */
	public static int[] sommeDeTableau(int[] tab1, int[] tab2) {
		if(tab1.length != tab2.length) {
			return null;
		}
		
		int[] resultat = new int[tab1.length];
		for(int i=0;i<tab1.length;i++) {
			resultat[i] = tab1[i] + tab2[i];
		}
		
		return resultat;
	}
	
	/**
	 * Calcul la somme des nombres contenu dans un tableau
	 * 
	 * @param tab1
	 * @param tab2
	 * @return le tableau des sommes.
	 * @throws IllegalArgumentException si les tableaux n'ont pas la même taille.
	 */
	public static int[] sommeDeTableauIdeal(int[] tab1, int[] tab2) throws IllegalArgumentException{
		if(tab1.length != tab2.length) {
			throw new IllegalArgumentException("Les 2 tableaux doivent être de même taille !");
		}
		
		int[] resultat = new int[tab1.length];
		for(int i=0;i<tab1.length;i++) {
			resultat[i] = tab1[i] + tab2[i];
		}
		
		return resultat;
	}
	
	
	/**
	 * Verifie si la somme des entiers contenu dans un tableau est superieur à un nombre donné. 
	 * @param tab 
	 * @param nombre maximum
	 * @return vrai si la somme est supérieure.
	 */
	public static boolean sommeSuperieurA(int[] tab, int nombre) {
		long somme = 0;
		for(int i:tab) {
			somme += i;
			if(somme > nombre) {
				return true;
			}
		}
		
		return false;
	}
	
}
