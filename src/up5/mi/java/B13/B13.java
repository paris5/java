package up5.mi.java.B13;

import java.util.Arrays;

public class B13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] tab1 = new int[] {1, 3, 5, 7, 9, 11, 12};
		int[] tab2 = new int[] {1, 2, 3, 4, 5, 6, 7};
		int[] tab3 = new int[] {1, 2, 3};
 		
		
		System.out.println(MonUtilTab.nombreDePair(tab1));
		System.out.println(MonUtilTab.nombreDePair(tab2));
		
		System.out.println(Arrays.toString(MonUtilTab.sommeDeTableau(tab1, tab2)));
		System.out.println(Arrays.toString(MonUtilTab.sommeDeTableau(tab1, tab3)));
		
		System.out.println(MonUtilTab.sommeSuperieurA(tab1, 10));
		System.out.println(MonUtilTab.sommeSuperieurA(tab1, 100));
		
	}

}
